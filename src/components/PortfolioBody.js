import React, {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap';
import {Col} from 'react-bootstrap';
import {Container} from 'react-bootstrap';
import {Card} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import {Modal} from 'react-bootstrap';
import Aos from 'aos';
import "aos/dist/aos.css";

const PortfolioBody = ({title,image,details,imageClass,link, modal}) => {

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
        Aos.init({
            duration: 500
        });

    }, [] );
    return(
        <>
            <Container>
                <Row data-aos="zoom-in" className="pb-5">
                    <Col md={6} className="pt-5 text-center" > 
                        <img src={image} className={`img-fluid ${imageClass}`} alt="Capstone 1"/>
                    </Col>
                    <Col md={6} className="pt-5">
                    <Card className="text-center projects-card bg-transparent">
                        <Card.Body>
                            <Card.Title className="text-dark h3 card--header">{title}</Card.Title>
                            <Card.Text className="text-muted">
                                {details}
                            </Card.Text> 
                            {modal ? 
                            <>
                            <Button className="projects-button" onClick={handleShow}>Visit Site
                            </Button> 
                            <Modal show={show} onHide={handleClose}>
                                <Modal.Header closeButton>
                                <Modal.Title className="text-dark">Capstone Project 3</Modal.Title>
                                </Modal.Header>
                                <Modal.Body className="text-muted">This page should contain preview of the project. This is just a placeholder. We will update this section once the project is available.</Modal.Body>
                                <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Close
                                </Button>
                                </Modal.Footer>
                            </Modal>                                                       
                            </>
                            : 
                            <a href={link} className="projects-button btn-link" target="_blank" rel="noreferrer">Visit Site</a>
                            }                                                                                  
                        </Card.Body>
                    </Card>                        
                    </Col>
                </Row>                                 
            </Container>    
        </>
    )

}

export default PortfolioBody;