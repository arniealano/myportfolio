import React from 'react';
import {Row} from 'react-bootstrap';
import {Col} from 'react-bootstrap';
import {Container} from 'react-bootstrap';

const PortfolioHeader = () => {
    return(
        <div className="portfolioSectionHeader">
            <Container>
                <Row>
                    <Col className="text-center py-5">
                        <h1 className={`display-5 portfolio--header`}>My Projects</h1>
                        <p className={`lead portfolio--text`}>Feel free to check each apps below for your reference.</p>
                        <a href="#contact" className={`btn custom-button`}>Contact Me</a>
                    </Col>
                </Row>
            </Container>            
        </div>
    )
}

export default PortfolioHeader;