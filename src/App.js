import React from 'react';
import LandingPage from './pages/LandingPage';
import Portfolio from './pages/Portfolio';
import Contact from './pages/Contact';
import Footer from './components/Footer';

import './App.css';
const App = () =>{
  return(
    <>
       <LandingPage />
       <Portfolio />
       <Contact />
       <Footer />         
    </>
  )
}

export default App;