import React from 'react';
import {Row} from 'react-bootstrap';
import {Col} from 'react-bootstrap';
import {Form} from 'react-bootstrap';
import {Button} from 'react-bootstrap';


const Contact = () => {
    return(
        <div className="contact-section" id="contact">
            <Row>
                <Col>
                <div className="contact">
                    <div className="contact--form">
                        <h1 className="contact--form--header">Get in touch!</h1>
                        <Form>
                            <Form.Group controlId="fullName">
                                <Form.Control className="border-0 contact-form-inputs" type="text" placeholder="Full name" required/>
                            </Form.Group>
                            <Form.Group controlId="email">
                                <Form.Control className="border-0 contact-form-inputs" type="email" placeholder="Email address" required/>
                            </Form.Group>     
                            <Form.Group controlId="message">
                                <Form.Control className="border-0 contact-form-inputs" as="textarea" rows={5} placeholder="Enter your message here" required />
                            </Form.Group>      
                            <Button type="submit" className="contact-form-button">Send Email</Button>                 
                        </Form>
                        <div className="contact-other pt-5">
                            <a href="https://www.linkedin.com/in/arnie-alano-1045b51b6/" className="contact-other-item text-muted" target="_blank" rel="noreferrer">LinkedIn</a>   
                            <a href="https://gitlab.com/arniealano" className="contact-other-item text-muted" target="_blank" rel="noreferrer">GitLab</a>     
                        </div>  
                    </div>        
                </div>                
                </Col>
            </Row>           
        </div>
    )
}

export default Contact;