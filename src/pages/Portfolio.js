import React from 'react';
import PortfolioBody from '../components/PortfolioBody';
import PortfolioHeader from '../components/PortfolioHeader';
import PortfolioBackground from '../components/PortFolioBackground';
import capstone_1_image from '../assets/images/capstone-1.png';
import capstone_2_image from '../assets/images/capstone-2.png';
import capstone_3_image from '../assets/images/capstone-3-removebg-preview.png';

const Portfolio = () => {
    return(
        <>
            <PortfolioHeader />
            <PortfolioBackground>
                <PortfolioBody title='Web Developer Portfolio'
                            details='This project is a simple developer portfolio created using HTML and CSS with bootstrap.'
                            image={capstone_1_image}
                            imageClass='cards-image'
                            link='https://arniealano.gitlab.io/capstone-1/'
                            modal={false}/>
                <hr />
                <PortfolioBody title='Course Booking App'
                            details='An app that allow the users to register, login, create courses, modify courses, delete courses, reactivate courses and enroll to specific courses. This app was created using HTML, CSS, JavaScript as front-end; Mongoose, Express.js, Node.js as back-end; and MongoDB as database.'
                            image={capstone_2_image}
                            imageClass='cards-image'
                            link='https://arniealano.gitlab.io/coursebookingapp/index.html'
                            modal={false}/>
                <hr />     
                <PortfolioBody title='Capstone Project 3'
                            details='This incoming project will be created using Javascript Techologies such React.js, Next.js, Express.js and Node.js.This page is not yet available at this point.'
                            image={capstone_3_image}
                            imageClass=''
                            link='https://arniealano.gitlab.io/coursebookingapp/index.html'
                            modal={true}/>   
            </PortfolioBackground>                                            
        </>
    )
}

export default Portfolio;